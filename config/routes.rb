Rails.application.routes.draw do

  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect { |p, request| request.flash[:error] = 'Authentication failure! ';
                                    'sessions/new' }

  delete 'signout', to: 'sessions#destroy', as: 'signout'

  post 'tweets/create'

  get 'sessions/new', to: 'sessions#new', as: 'signin'

  delete 'sessions/destroy'

  get 'home/show'

  root 'home#show'

# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
