Rails.application.config.middleware.use OmniAuth::Builder do
  provider :twitter, ENV['twitter_consumer_key'], ENV['twitter_consumer_secret']
  OmniAuth.config.on_failure = Proc.new { |env|
    OmniAuth::FailureEndpoint.new(env).redirect_to_failure
  }

end
