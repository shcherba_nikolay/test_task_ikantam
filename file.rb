class A
  def initialize(name)
    @name = name
  end

  def say_name(obj)
    obj.name
  end

  protected

  def name
    puts @name
  end
end

b = A.new('first')
c = A.new('second')
b.say_name(c)