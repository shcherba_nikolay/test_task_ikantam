module SessionsHelper

  def current_user
    user_id = session[:user_id]
    @current_user ||= User.find_by(id: user_id)
  end

  def current_user=(user)
    @current_user = user
  end

  def signed_in?
    !current_user.nil?
  end

  def sign_out
    session.delete :user_id
    @current_user = nil
  end

  def signed_in_user
    redirect_to signin_path unless signed_in?
  end
end


