class TweetService
  require 'pry'
  require 'fileutils'
  attr_reader :tweet

  def initialize(user)
    @user = user
  end

=begin
  def post_to_twitter(params)
    @tweet = user.tweets.build(params)
    if @tweet.valid?
      begin
        if @tweet.tweet_picture.file?
          old_picture_path = @tweet.tweet_picture.path
          @tweet.tweet_picture.save
        end
        tweet_from_api = TwitterApiService.post(client, @tweet.status, @tweet.tweet_picture)
        @tweet.created_at= tweet_from_api.created_at
        @tweet.tweet_url= tweet_from_api.url.to_s
        @tweet.save
        if old_picture_path
          dir = @tweet.tweet_picture.path.match(/(.+)\/.+\z/)
          FileUtils.mkdir_p dir[1]
          FileUtils.mv(old_picture_path, @tweet.tweet_picture.path)
        end
      rescue
        @tweet.errors.add(:twitter_save_error, 'Can\'t send tweet!')
        @tweet.tweet_picture.destroy
        false
      end
    else
      false
    end
  end
=end

  def post_to_twitter(params)
    @tweet = user.tweets.build(params)
    if @tweet.valid?
      begin
        tweet_from_api = TwitterApiService.post(client, @tweet.status, @tweet.tweet_picture)
        @tweet.created_at= tweet_from_api.created_at
        @tweet.tweet_url= tweet_from_api.url.to_s
        @tweet.save
      rescue
        @tweet.errors.add(:twitter_save_error, 'Can\'t send tweet!')
        false
      end
    else
      false
    end
  end

  private

  def client
    @client ||= TwitterApiService.twitter_client(user)
  end

  def user
    @user
  end
end