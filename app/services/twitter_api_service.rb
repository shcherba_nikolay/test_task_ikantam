class TwitterApiService
  require 'pry'
  class << self
    def find_or_create_from_omniauth(omniauth_hash)
      user = User.where(provider: omniauth_hash.provider, user_id: omniauth_hash.uid).first_or_create
      user.update(
          name: omniauth_hash.info.nickname,
          oauth_token: omniauth_hash.credentials.token,
          oauth_secret: omniauth_hash.credentials.secret
      )
      user
    end

    def twitter_client(user)
      Twitter::REST::Client.new do |config|
        config.consumer_key = ENV['twitter_consumer_key']
        config.consumer_secret = ENV['twitter_consumer_secret']
        config.access_token = user.oauth_token
        config.access_token_secret = user.oauth_secret
      end
    end

    def post(client, status, tweet_picture)
      if !tweet_picture.file?
        tweet = client.update(status)
      else
        content = Paperclip.io_adapters.for(tweet_picture).read
        dir = tweet_picture.path.match(/(.+)\/.+\z/)[1]
        FileUtils.mkdir_p dir
        File.open(tweet_picture.path, 'w+b') { |f| f.write(content) }
        file = File.new(tweet_picture.path)
        begin
          tweet = client.update_with_media(status, file)
        rescue
          FileUtils.remove_dir dir
          raise StandardError
        end
        FileUtils.remove_dir dir
      end
      tweet
    end
  end
end

