class Tweet < ApplicationRecord
  belongs_to :user
  has_attached_file(:tweet_picture)
  validates_attachment_content_type :tweet_picture, content_type: /\Aimage\/jpeg|png|gif\z/

  validates :user_id, presence: true
  validates :status, presence: true, length: {maximum: 140}
end
