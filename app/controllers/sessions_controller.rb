class SessionsController < ApplicationController

  def new; end

  def create
    @user = TwitterApiService.find_or_create_from_omniauth(omniauth_hash)
    session[:user_id] = @user.id
    flash[:success] = 'You signed in!'
    redirect_to home_show_path
  end

  def destroy
    sign_out
    flash[:success] = 'You signed out from application'
    redirect_to signin_path
  end

  private

  def omniauth_hash
    request.env['omniauth.auth']
  end
end
