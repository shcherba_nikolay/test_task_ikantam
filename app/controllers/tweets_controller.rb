class TweetsController < ApplicationController
  before_action :signed_in_user

  def create
    tweet_service = TweetService.new(current_user)
    if tweet_service.post_to_twitter(tweet_params)
      flash[:success] = 'You add the tweet'
      redirect_to root_path
    else
      @tweet = tweet_service.tweet
      respond_to do |format|
        format.html do
          @tweets = current_user.tweets.all
          render 'home/show'
        end
        format.js
      end
    end
  end

  private

  def tweet_params
    params.require(:tweet).permit(:status, :tweet_picture)
  end
end
