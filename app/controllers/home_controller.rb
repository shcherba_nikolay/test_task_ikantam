class HomeController < ApplicationController
  before_action :signed_in_user

  def show
    @tweet = current_user.tweets.build
    @tweets = current_user.tweets.all
  end
end
