require 'rails_helper'

RSpec.describe Tweet, type: :model do

  let(:user) { FactoryBot.create(:user) }
  before do
    @tweet = user.tweets.build(status: 'Your tweet',
                               tweet_picture: fixture_file_upload('files/jpg_image.jpg', 'image/jpeg'))
  end
  subject { @tweet }
  it { is_expected.to respond_to :status }
  it { is_expected.to respond_to :user }
  it { is_expected.to respond_to :tweet_url }
  it { is_expected.to respond_to :created_at }
  it { is_expected.to respond_to :tweet_picture }
  it { is_expected.to be_valid }
  specify { expect(@tweet.user).to eq user }

  describe 'should be valid with gif file' do
    before do
      @tweet.tweet_picture = fixture_file_upload('files/gif_image.gif', 'image/gif')
    end
    it { is_expected.to be_valid }
  end

  describe 'should be valid with png file' do
    before do
      @tweet.tweet_picture = fixture_file_upload('files/png_image.png', 'image/png')
    end
    it { is_expected.to be_valid }
  end

  describe 'should not to be valid with pdf file' do
    before do
      @tweet.tweet_picture = fixture_file_upload('files/pdf_file.pdf', 'application/vnd.openxmlformats')
    end
    it { is_expected.not_to be_valid }
  end

end
