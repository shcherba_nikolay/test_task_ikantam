FactoryBot.define do
  factory :tweet do
    status 'Your status'
    tweet_picture { fixture_file_upload('files/png_image.png', 'image/png') }
  end
end
