require 'rails_helper'

RSpec.describe 'Authentications', type: :request do

  describe 'sign-in page', :type => :feature do
    subject { page }
    before { visit '/' }
    it { is_expected.to have_selector('form.button_to') }
    it { is_expected.not_to have_link('Sign out') }
  end

  describe 'for not signed in users' do
    it 'should redirect to signin path' do
      get '/home/show'
      expect(response).to redirect_to signin_path
    end


    it 'should redirect to signin path' do
      get '/'
      expect(response).to redirect_to signin_path
    end

    it 'should redirect to signin path' do
      post '/tweets/create'
      expect(response).to redirect_to signin_path
    end


    it 'should returns http success' do
      get signin_path
      expect(response).to have_http_status(:success)
    end
  end

  describe 'for signed in user' do

    describe 'to create ' do
      before do
        Rails.application.env_config['omniauth.auth'] = OmniAuth.config.mock_auth[:twitter]
        get '/auth/twitter/callback'
      end
      specify { expect(response).to redirect_to home_show_path }

      describe 'signout' do

        specify 'delete method' do
          delete '/sessions/destroy'
          expect(response).to redirect_to signin_path
        end
      end
    end
  end
end
