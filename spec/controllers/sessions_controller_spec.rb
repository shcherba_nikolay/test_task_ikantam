require 'rails_helper'

RSpec.describe SessionsController, type: :controller do
  describe ':create' do
    before do
      request.env['omniauth.auth'] = OmniAuth.config.mock_auth[:twitter]
      Rails.application.routes.draw do
        get '/sessions/create' => 'sessions#create'
        get 'home/show'
      end
    end
    after do
      Rails.application.reload_routes!
    end
    specify { expect { get :create}.to change(User, :count).by(1) }
  end


end
