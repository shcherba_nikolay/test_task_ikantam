require 'rails_helper'

RSpec.feature "HomePages", type: :feature do
  let(:user) { FactoryBot.create(:user) }
  describe 'home page' do
    subject { page }
    before do
      page.set_rack_session(user_id: user.id)
      visit('/')
    end
    it { is_expected.to have_content('Home page') }
    it { is_expected.not_to have_selector('form.button_to') }
    it { is_expected.to have_link('Sign out') }
    it { is_expected.to have_field('tweet_status') }
    it { is_expected.to have_field('tweet_tweet_picture') }
    it { is_expected.to have_button('Tweet!') }

    describe 'send tweet' do
      describe 'without file' do
        describe 'valid status' do
          before do
            fill_in 'tweet_status', with: 'Tweet text'
          end
          specify { expect { click_button 'Tweet!' }.to change(Tweet, :count).by(1) }
        end

        describe 'add tweet' do
          before do
            fill_in 'tweet_status', with: 'Tweet text'
            click_button 'Tweet!'
            @tweet = user.tweets.last
          end
          it { is_expected.to have_selector('.alert-success', text: 'You add the tweet') }
          it { is_expected.to have_content(@tweet.status) }
          it { is_expected.to have_link('View', href: @tweet.tweet_url) }
          it { is_expected.to have_content(@tweet.created_at.localtime.strftime "%d.%m.%Y %H:%M") }
        end

        describe 'invalid status' do
          specify { expect { click_button 'Tweet!' }.to_not change(Tweet, :count) }
        end

        describe 'add tweet' do
          before do
            fill_in 'tweet_status', with: 'Tweet text'
            click_button 'Tweet!'
          end
          it { is_expected.to have_selector('.alert-success', text: 'You add the tweet') }
          it { is_expected.not_to have_selector('.form_errors') }
        end
      end

      describe 'with file' do
        describe 'valid file' do
          before do
            fill_in 'tweet_status', with: 'Tweet text'
            page.attach_file('tweet_tweet_picture', './spec/fixtures/files/png_image.png')
          end
          specify { expect { click_button 'Tweet!' }.to change(Tweet, :count).by(1) }
        end

        describe 'add tweet' do
          before do
            fill_in 'tweet_status', with: 'Tweet text'
            page.attach_file('tweet_tweet_picture', './spec/fixtures/files/png_image.png')
            click_button 'Tweet!'
            @tweet = user.tweets.last
          end
          it { is_expected.to have_selector('.alert-success', text: 'You add the tweet') }
          it { is_expected.to have_content(@tweet.status) }
          it { is_expected.to have_link('View', href: @tweet.tweet_url) }
          it { is_expected.to have_content(@tweet.created_at.localtime.strftime("%d.%m.%Y %H:%M")) }
        end

        describe 'invalid file' do
          before do
            fill_in 'tweet_status', with: 'Tweet text'
            page.attach_file('tweet_tweet_picture', './spec/fixtures/files/pdf_file.pdf')
            click_button 'Tweet!'
          end
          it { is_expected.not_to have_selector('.alert-success', text: 'You add the tweet') }
          it { is_expected.to have_selector('.form_errors') }
        end
      end
    end
  end
end

