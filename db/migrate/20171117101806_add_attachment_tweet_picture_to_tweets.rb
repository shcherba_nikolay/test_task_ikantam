class AddAttachmentTweetPictureToTweets < ActiveRecord::Migration[5.1]
  def self.up
    change_table :tweets do |t|
      t.attachment :tweet_picture
    end
  end

  def self.down
    remove_attachment :tweets, :tweet_picture
  end
end
